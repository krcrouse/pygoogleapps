[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# pygoogleapps

The pygoogleapps package (googleapps module) is a Python library that provides an object-oriented abstraction to the Google(tm) Python-eligible APIs, as per here: https://console.cloud.google.com/apis/library. This package provides simpler credentialing and a more pythonic interface than currently available through Google's offerings in the idea of standalone programs and scripts running for a single user. This is not intended for building platforms in which many users are using the application simultaneously.

pygoogleapps and its developers have no relationship with Google, Alphabet, or the Google Cloud Platform. We are merely building modules that interface with the services provided.

# Stability

The current release is alpha software.  It has been released for broader use among a group of colleagues. Your mileage may vary. The interface will certainly change, and a much more dynamic and responsive version is in the works - if you are interested in this work, stay tuned for that.


## Distribution
* [GitLab Project](https://gitlab.com/krcrouse/pygoogleapps)
* [PyPI Distribution Page](https://pypi.org/project/pygoogleapps)


## Project Status

Currently, pygoogleapps has been used in practice but is shallowly vetted condition and should be considered **alpha** software.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install pygoogleapps.

```bash
pip install pygoogleapps
```

## Usage

```
coming soon
```

## Contributing
Contributions are collaboration is welcome. For major changes, please contact me in advance to discuss.

Please make sure to update tests for any contribution, as appropriate.

## Author

[Kevin Crouse](mailto:krcrouse@gmail.com). Copyright, 2021.

## License
[Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0)

# Acknowledgements

pygoogleapps and its developers have no relationship with Google, Alphabet, or the Google Cloud Platform.
